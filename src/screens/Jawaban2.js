import React, {useContext} from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {RootContext} from './index';

const Jawaban2 = () => {
  const state = useContext(RootContext);

  const showList = ({item, index}) => {
    return (
      <View>
        <View style={styles.list}>
          <Text>{item.name}</Text>
          <Text>{item.position}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{flex: 1, paddingHorizontal: 10}}>
        <FlatList
          data={state.name}
          renderItem={showList}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    padding: 20,
    paddingRight: 100,
    borderWidth: 5,
    borderRadius: 5,
    borderColor: '#ededed',
    marginBottom: 10,
  },
});

export default Jawaban2;
