/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Jawaban1 from './src/screens/Jawaban1';
import Jawaban2 from './src/screens/index';

AppRegistry.registerComponent(appName, () => Jawaban2);
